package hasher

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"strings"
)

//	This package is going to be used to hash the byte array of the frame.

//	External Hash Functions
func HashIt(arr []byte, last string) string { // Hashes a byte array + string with sha-256
	t := string(arr[:]) + last
	text := Hash_sha256(t)
	return text
}

func Hash_sha256(text string) string {
	h := sha256.New()
	h.Write([]byte(text))
	bs := h.Sum(nil)
	str := hex.EncodeToString(bs)
	return str
}

func Hash_md5(text string) string {
	h := md5.New()
	h.Write([]byte(text))
	bs := h.Sum(nil)
	str := hex.EncodeToString(bs)
	return str
}

//	Internal Functions
func intArrToStringArr(arr []int) []string {
	vals := []string{}
	for i := 0; i < len(arr); i++ {
		vals = append(vals, strconv.Itoa(arr[i]))
	}
	return vals
}

func stringArrToString(arr []string) string {
	text := "[" + strings.Join(arr, ", ") + "]"
	return text
}

func intArrToString(arr []int) string {
	vals := intArrToStringArr(arr)
	text := stringArrToString(vals)
	return text
}
