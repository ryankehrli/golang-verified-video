package cryptox

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
)

//	Contains Private/Public Key Pair
type Keys struct {
	PrivateKey *rsa.PrivateKey
	PublicKey  rsa.PublicKey
}

//	Class Function to Generate New KeyPairs
func (object *Keys) GenerateKeys() {
	a, b := MakeKeys()
	object.PrivateKey = a
	object.PublicKey = b
}

//	Creates New Keys and Returns Them.
func MakeKeys() (*rsa.PrivateKey, rsa.PublicKey) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		fmt.Println("Error generating keypair: ", err)
		panic(err)
	}
	publicKey := privateKey.PublicKey

	return privateKey, publicKey
}
