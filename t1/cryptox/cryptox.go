package cryptox

//	Primary RSA struct
type Cryptox struct {
	keys Keys
}

func New() *Cryptox {
	return &Cryptox{}
}

//	String Encryption Functions
func (c *Cryptox) Encrypt(text string) string {

	return ""
}

func (c *Cryptox) Decrypt(text string) string {

	return ""
}

//	Generate Keys for RSA Object
func (c *Cryptox) GenerateKeys() {
	c.keys.GenerateKeys()
}

// func main() {
// 	d := Crypto{x}
// 	d.GenerateKeys()
// 	fmt.Println(d)
// }
