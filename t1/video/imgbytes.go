package main

import (
	"fmt"

	"bitbucket.org/ryankehrli/golang-verified-video/t1/hasher"
	"gocv.io/x/gocv"
)

//	This file is converting the webcam feed into bytes, then converting the bytes back
//	into an image. It is also hashing the byte array for each frame.

type buffer []byte

type frame struct {
	buffer []byte
	frames []buffer
}

func main() {
	webcam, _ := gocv.VideoCaptureDevice(0)
	window := gocv.NewWindow("Bytes")

	img := gocv.NewMat()

	thing := frame{}
	count := 0
	//	Main Loop
	for {
		//	Controls length of Video 120frames/30fps = 4 seconds
		if count > 120 {
			break
		}
		webcam.Read(&img)

		//	Creates a byte representation of the image and adds it to the frames record
		thing.buffer = img.ToBytes()
		thing.frames = append(thing.frames, thing.buffer)
		//stuff := string(thing.buffer[:])
		//fmt.Println(hasher.Hash_sha256(stuff))
		key := hasher.HashIt(thing.buffer)
		fmt.Println(key)

		//	Converts the bytes back into an image
		imgb, err := gocv.NewMatFromBytes(img.Rows(), img.Cols(), img.Type(), thing.buffer)
		if err != nil {
			fmt.Println("Error: ", err)
		}

		window.IMShow(imgb)
		window.WaitKey(1)

		count++
	}
	img.Close()
}
