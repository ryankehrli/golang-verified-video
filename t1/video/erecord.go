package main

import (
	"fmt"

	"bitbucket.org/ryankehrli/golang-verified-video/t1/hasher"
	"gocv.io/x/gocv"
)

//	This file is converting the webcam feed into bytes, then converting the bytes back
//	into an image. It is also hashing the byte array for each frame.
//	It will save the video and the hashes of each frame in a text file.

type buffer []byte

type frame struct {
	buffer []byte
	frames []buffer

	hashes []string
}

func main() {
	//	VideoCaptureDevice
	webcam, err := gocv.VideoCaptureDevice(0)
	if err != nil {
		fmt.Printf("Error opening video capture device: %v\n", err)
		return
	}
	defer webcam.Close()

	//	The Viewing Window
	window := gocv.NewWindow("Webcam")
	defer window.Close()

	//	img Global Vairable, updated by videocapturedevice in a loop to produce video.
	img := gocv.NewMat()
	defer img.Close()
	webcam.Read(&img) // Necessary for the writer to be able to initialize correctly.

	//	Variables
	thing := frame{}
	count := 0
	last := "start value"
	videoFile := "../data/example.avi"
	//hashFile := "../data/hash.txt"

	//	Video-File-Writer
	writer, err := gocv.VideoWriterFile(videoFile, "MJPG", 30, img.Cols(), img.Rows(), true)
	if err != nil {
		fmt.Printf("error opening video writer device: %v\n", err)
		return
	}
	defer writer.Close()

	//	Main Loop
	for {
		//	Controls length of Video 120frames/30fps = 4 seconds
		if count > 120 {
			break
		}
		//	Webcam reads and outputs to global img variable if it can.
		if ok := webcam.Read(&img); !ok {
			fmt.Println("Webcam read error.")
			return
		}
		if img.Empty() {
			continue
		}

		//	Creates a byte representation of the image and adds it to the frames record
		thing.buffer = img.ToBytes()
		thing.frames = append(thing.frames, thing.buffer)

		//	Creates the hash (bytes + last frame)
		key := hasher.HashIt(thing.buffer, last)
		thing.hashes = append(thing.hashes, key)

		window.IMShow(img)
		window.WaitKey(1)
		writer.Write(img)

		count++
		last = key
	}

	//	Listing hashes in frame object
	for i := 0; i < len(thing.hashes); i++ {
		fmt.Printf("\nFrame%d: %s", i, thing.hashes[i])
	}
	fmt.Println()
}
