package main

import (
	"fmt"

	"gocv.io/x/gocv"
)

func main() {

	deviceID := 0
	saveFile := "../data/example.avi"

	webcam, err := gocv.OpenVideoCapture(deviceID)
	if err != nil {
		fmt.Printf("Error opening video capture device: %v\n", deviceID)
		return
	}
	defer webcam.Close()

	img := gocv.NewMat()
	defer img.Close()

	if ok := webcam.Read(&img); !ok {
		fmt.Printf("Cannot read device %v\n", deviceID)
		return
	}

	writer, err := gocv.VideoWriterFile(saveFile, "MJPG", 30, img.Cols(), img.Rows(), true)
	if err != nil {
		fmt.Printf("error opening video writer device: %v\n", saveFile)
		return
	}
	defer writer.Close()

	for i := 0; i < 120; i++ {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("Device closed: %v\n", deviceID)
			return
		}
		if img.Empty() {
			continue
		}

		fmt.Printf("\nCols: ", img.Cols(), "\nRows: ", img.Rows())

		writer.Write(img)
	}
}
