package main

import (
	"fmt"

	"gocv.io/x/gocv"
)

func main() {

	filename := "../data/example.avi"
	window := gocv.NewWindow("Video")

	img := gocv.IMRead(filename, gocv.IMReadColor)
	if img.Empty() {
		fmt.Printf("Error reading image from: %v\n", filename)
		return
	}
	for {
		window.IMShow(img)
		if window.WaitKey(1) >= 0 {
			break
		}
	}
}
