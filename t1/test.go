package main

import (
	"fmt"

	"bitbucket.org/ryankehrli/golang-verified-video/t1/cryptox"
)

func main() {
	d := cryptox.New()
	fmt.Println(d)
}
